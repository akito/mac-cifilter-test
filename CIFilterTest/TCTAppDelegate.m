//
//  TCTAppDelegate.m
//  CIFilterTest
//
//  Created by Akito Nozaki on 10/18/13.
//  Copyright (c) 2013 tanoshi. All rights reserved.
//


#import "TCTAppDelegate.h"
#import "TCTMainAppView.h"

@interface TCTAppDelegate ()

@property (weak) IBOutlet TCTMainAppView *mainView;

@property CIFilter *filter;

@property NSImage *originalImage;
@property NSImage *filteredImage;
@property NSImage *filteredImage2;

@end

@implementation TCTAppDelegate {
	u_int8_t _table[256];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// force layer so our image shows up
	[self.mainView setWantsLayer:YES];

	[self.mainView.tabView setDelegate:self];

	// create the filter and set the gradient image key here.
	self.filter = [CIFilter filterWithName:@"CIColorMap"];
	[self.filter setValue:[self createGradientImage] forKey:kCIInputGradientImageKey];
}

/**
 * We will create a gradient image based on the table we created earlier. The image should be leaner 0 to 255.
 */
- (CIImage *)createGradientImage {
	for (int i = 0; i < 256; i++) {
		_table[i] = (u_int8_t) i;
	}

	size_t bitsPerComponent = 8;
	size_t bitsPerPixel = bitsPerComponent * 1;
	size_t width = 256;
	size_t height = 1;

	CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceGray();
	CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault | kCGImageAlphaNone;
	CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;

	CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, _table, 256, NULL);

	CGImageRef imageRef = CGImageCreate(width, height, bitsPerComponent, bitsPerPixel, width * bitsPerPixel / bitsPerComponent, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
	// we are setting the kCIImageColorSpace to null here so nothing really messes with it. Not sure if this is necessary but just in case.
	CIImage *image = [[CIImage alloc] initWithCGImage:imageRef options:@{kCIImageColorSpace : [NSNull null]}];

	// cleanup
	CGDataProviderRelease(provider);
	CGColorSpaceRelease(colorSpaceRef);
	CGImageRelease(imageRef);

	return image;
}

- (void)testWithImage:(NSImage *)image {
	// ----------------------------------------
	// unmodified image for reference
	self.originalImage = image;

	// ----------------------------------------
	// we are setting the color space to null so it doesn't try to do anything funny here.
	CIImage *inputImage = [[CIImage alloc] initWithData:[image TIFFRepresentation] options:@{kCIImageColorSpace : [NSNull null]}];
	[_filter setValue:inputImage forKey:kCIInputImageKey];

	CIImage *outputImage = [_filter valueForKey:kCIOutputImageKey];

	CGContextRef contextRef = [[NSGraphicsContext currentContext] graphicsPort];
	// we are also setting the kCIContextWorkingColorSpace to null so the color stays in tacked.
	CIContext *ciContext = [CIContext contextWithCGContext:contextRef options:@{kCIContextWorkingColorSpace : [NSNull null]}];
	CGImageRef cgImage = [ciContext createCGImage:outputImage fromRect:[outputImage extent]];

	// save the resulting image.
	self.filteredImage = [[NSImage alloc] initWithCGImage:cgImage size:[outputImage extent].size];

	// this method works in 10.8 but does not work in 10.9

	// Also to note this seems to work every once and a while... making it seem like a race condition.

	// ----------------------------------------
	// our alternative.
	NSBitmapImageRep *rep = [[NSBitmapImageRep alloc] initWithCIImage:outputImage];
	self.filteredImage2 = [[NSImage alloc] init];
	[self.filteredImage2 addRepresentation:rep];

	// this method works in 10.9 but does not work in 10.8

	self.mainView.originalImage = self.originalImage;
	self.mainView.filteredImage = self.filteredImage;
    self.mainView.filteredImage2 = self.filteredImage2;

	// cleanup
	[_filter setValue:nil forKey:kCIInputImageKey];
}

- (void)tabView:(NSTabView *)tabView didSelectTabViewItem:(NSTabViewItem *)tabViewItem {
	// make sure the image gets re-drawn to the screen.
	[self.mainView setNeedsDisplay:YES];
}


- (IBAction)testImageButton:(id)sender {
	// Test image found online for testing... This is the best image to check to see if image is getting manipulated.
	NSImage *image = [NSImage imageNamed:@"photo_no_exif"];
	[self testWithImage:image];
}

- (IBAction)selectImageButton:(id)sender {
	// test with custom image to see if this really makes any difference.
	NSOpenPanel *panel = [NSOpenPanel openPanel];

	[panel setAllowsMultipleSelection:NO];
	[panel setCanChooseFiles:YES];
	[panel setCanChooseDirectories:NO];
	[panel setAllowedFileTypes:[NSImage imageFileTypes]];
	[panel beginWithCompletionHandler:^(NSInteger result) {
		if (result == NSFileHandlingPanelOKButton) {
			NSImage *image = [[NSImage alloc] initWithContentsOfURL:[panel URL]];
			[self testWithImage:image];
		}

	}];
}

@end