//
//  TCTAppDelegate.h
//  CIFilterTest
//
//  Created by Akito Nozaki on 10/18/13.
//  Copyright (c) 2013 tanoshi. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TCTAppDelegate : NSObject <NSApplicationDelegate, NSTabViewDelegate>

@property (assign) IBOutlet NSWindow *window;

@end