//
//  TCTMainAppView.h
//
// Created by Akito Nozaki on 10/18/13.
//



/**
 * @brief TODO
 *
 * @author Akito Nozaki 
 * @date 10/18/13 originally created date
 */
@interface TCTMainAppView : NSView

@property(weak) IBOutlet NSTabView *tabView;

@property(nonatomic) NSImage *originalImage;
@property(nonatomic) NSImage *filteredImage;
@property(nonatomic) NSImage *filteredImage2;

@end