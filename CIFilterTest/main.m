//
//  main.m
//  CIFilterTest
//
//  Created by Akito Nozaki on 10/18/13.
//  Copyright (c) 2013 tanoshi. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}