//
//  TCTMainAppView.m
//
// Created by Akito Nozaki on 10/18/13.
//


#import "TCTMainAppView.h"

@interface TCTMainAppView()
@property (weak) IBOutlet NSView *originalImageView;
@property (weak) IBOutlet NSView *ciFilteredImageView;
@property (weak) IBOutlet NSView *hackImageView;
@property (weak) IBOutlet NSView *manuallyFilteredImageView;
@end

@implementation TCTMainAppView

#pragma mark - Init

- (id)initWithFrame:(NSRect)frameRect {
	self = [super initWithFrame:frameRect];
	if (self) {
		// change redraw policy so the updateLayer gets called when we need it too.
		[self setLayerContentsRedrawPolicy:NSViewLayerContentsRedrawOnSetNeedsDisplay];
	}

	return self;
}


#pragma mark - Layer

- (BOOL) wantsUpdateLayer {
	return YES;
}

- (void)updateLayer {
	self.originalImageView.layer.contents = self.originalImage;
	self.originalImageView.layer.contentsGravity = kCAGravityResizeAspect;

	self.ciFilteredImageView.layer.contents = self.filteredImage;
	self.ciFilteredImageView.layer.contentsGravity = kCAGravityResizeAspect;
    
    self.hackImageView.layer.contents = self.filteredImage2;
    self.hackImageView.layer.contentsGravity = kCAGravityResizeAspect;
}

#pragma mark - Getter/Setters

- (void)setOriginalImage:(NSImage *)originalImage {
	_originalImage = originalImage;
	[self setNeedsDisplay:YES];
}

- (void)setFilteredImage:(NSImage *)filteredImage {
	_filteredImage = filteredImage;
	[self setNeedsDisplay:YES];
}
- (void)setFilteredImage2:(NSImage *)filteredImage2 {
    _filteredImage2 = filteredImage2;
    [self setNeedsDisplay:YES];
}

@end