CIFilterTest
============

This project is created to show how CIImage processed with CIFilter displays differently in 10.8 vs 10.9.

About
-----

This application simply takes a image and applies a simple CIFilter to a test image and displays it on screen. The
application displays 3 images.

### Original Image

Original image is simply opened by NSImage and assigned to a CALayer's content for display. The aspect ratio is kept the
same to make comparing the images easier.

### Filtered Image

Filtered Image runs the original image through the CIFilter. Making sure kCIImageColorSpace is always set to nil at all
steps to prevent any ColorSpace getting applied to the image. Then the output of this filter, resulting CIImage, is used
to create a NSImage using "initWithCGImage:size:",

This method works correctly in 10.8 but does not work properly in 10.9

### Filtered Image 2

Mostly the same as above, but instead of using CIImage to CGImage to NSImage. We create a NSBitmapImageRep out of CIImage
and assign the representation to the NSImage.

This method works correctly in 10.9 but does not work properly in 10.8

Problem
-------

The problem is that I have not found a way to get the display on 10.8 and 10.9 to match up. On 10.8 "Filtered Image"
method works correctly but does not in 10.9. Then when I use "Filtered Image 2" method the image comes out correctly
in 10.9 but does not in 10.8.

Another interesting thing to note here is if I export the image using "Filtered Image 2" method, the output image is the
same (Using representationUsingType:properties).